using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService service;

        public ParkingController(IParkingService service)
        {
            this.service = service;
        }

        [ActionName("balance")]
        [HttpGet]
        public decimal GetParkingBalance()
        {
            return service.GetBalance();
        }

        [ActionName("capacity")]
        [HttpGet]
        public int GetParkingCapacity()
        {
            return service.GetCapacity();
        }

        [ActionName("freePlaces")]
        [HttpGet]
        public int GetFreePlaces()
        {
            return service.GetFreePlaces();
        }
    }
}