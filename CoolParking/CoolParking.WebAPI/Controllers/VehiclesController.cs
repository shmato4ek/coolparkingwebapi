using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Nancy.Json;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService service;

        public VehiclesController(IParkingService service)
        {
            service = new ParkingService();
            this.service = service;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<List<VehicleModel>> GetVehicles()
        {
            return Ok(service.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleModel> GetVehicle(string id)
        {
            return Ok(service.TakeVehicle(id));
        }

        [HttpPost]
        public ActionResult<VehicleModel> Post([FromBody] VehicleModel vehicle)
        {
            string id = vehicle.Id.ToString();
            int typeNumber = vehicle.VehicleType;
            VehicleType type = service.GetVehicleType(typeNumber);
            decimal balance = vehicle.Balance;
            Vehicle newVehicle = new Vehicle(id, type, balance);
            service.AddVehicle(newVehicle);
            return Created("Post", newVehicle);
        }

        [HttpDelete]
        public ProducesResponseTypeAttribute Delete([FromBody] VehicleModel vehicle)
        {
            string id = vehicle.Id;
            service.DeleteVehicle(id);
            return null;
        }
    }
}