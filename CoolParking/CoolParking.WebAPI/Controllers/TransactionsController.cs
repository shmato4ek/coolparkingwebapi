using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService service;

        public TransactionsController(IParkingService service)
        {
            this.service = service;
        }

        [ActionName("last")]
        [HttpGet]
        public TransactionInfo GetLastTransaction()
        {
            return service.GetLastTransaction();
        }

        [ActionName("all")]
        [HttpGet]
        public string GetAllTransactionsFromLog()
        {
            return service.ReadFromLog();
        }

        [ActionName("topUpVehicle")]
        [HttpPut]
        public ActionResult<VehicleModel> TopUpVehicle([FromBody] TransactionModel transaction)
        {
            string id = transaction.Id;
            decimal sum = transaction.Sum;
            service.TopUpVehicle(id, sum);
            var vehicle = service.TakeVehicle(id);
            return Ok(vehicle);
        }
    }
}