using System;
using System.Collections.Generic;
using CoolParking.BL.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class TransactionModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }

        [JsonProperty("Date")]
        public DateTime Date { get; set; }
    }
}