using System;
using System.Collections.Generic;
using CoolParking.BL.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class VehicleModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}