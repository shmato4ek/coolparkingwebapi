﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Settings
    {
        private static Settings _instance;
        public decimal StarterBalance { get; private set; }
        public int ParkingCapacity { get; private set; }
        public double PaymentDebitingPeriod { get; private set; }
        public double LoggingPeriod { get; private set; }
        public Dictionary<VehicleType, decimal> Prices { get; private set; }
        public decimal PenaltyRate { get; private set; }
        private Settings() { }
        public static Settings GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Settings();
                _instance.StarterBalance = 0m;
                _instance.ParkingCapacity = 10;
                _instance.PaymentDebitingPeriod = 5000;
                _instance.LoggingPeriod = 60000;
                _instance.Prices = new Dictionary<VehicleType, decimal>();
                _instance.Prices.Add(VehicleType.PassengerCar, 2m);
                _instance.Prices.Add(VehicleType.Truck, 5m);
                _instance.Prices.Add(VehicleType.Bus, 3.5m);
                _instance.Prices.Add(VehicleType.Motorcycle, 1m);
                _instance.PenaltyRate = 2.5m;
            }
            return _instance;
        }
        public void SetPaymentDebitingPeriod(double seconds)
        {
            PaymentDebitingPeriod = seconds;
        }
        public void SetLoggingPeriod(double seconds)
        {
            LoggingPeriod = seconds;
        }
    }
}