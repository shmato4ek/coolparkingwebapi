﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.BL.Util
{
    public class LogModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogService>().To<LogService>();
        }
    }
}
