﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string Path { get; set; }
        public string LogPath => Path;

        public LogService(string path = null)
        {
            Path = @"C:\Users\User\source\repos\CoolParking\Logs\logs.txt";
            if (path != null)
            {
                Path = path;
            }
        }

        public string Read()
        {
            try
            {
                StreamReader sr = new StreamReader(Path);
                string log = sr.ReadToEnd();
                sr.Dispose();
                return log;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            try
            {
                StreamWriter sw = new StreamWriter(Path, true, System.Text.Encoding.Default);
                sw.WriteLine(logInfo);
                sw.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}