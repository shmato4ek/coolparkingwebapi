﻿using System;
using CoolParking.WebAPI;
using System.Threading.Tasks;
using System.Net.Http;
using CoolParking.BL.Models;
using System.Net.Http.Json;

namespace CoolParking.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("All procedures:\n\n");
                Console.WriteLine("1) Check parking balance.\n" +
                    "2) Check parking capacity.\n" +
                    "3) Check free places.\n" +
                    "4) Check all vehicles in Parking.\n" +
                    "5) Check concrete vehicle by id\n" +
                    "6) Post new vehicle.\n" + 
                    "7) Delete concrete vehicle.\n" +
                    "8) Get last transactions.\n" +
                    "9) Get all transactions.\n" +
                    "10) Top up concrete vehicle.\n" +
                    "0) Exit.\n\n");
                Console.WriteLine("Write the number of procedure and push Enter: ");
                int answer = Int32.Parse(Console.ReadLine());
                switch (answer)
                {
                    case 1:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync("https://localhost:44359/api/parking/balance");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nBalance: " + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 2:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync("https://localhost:44359/api/parking/capacity");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nCapacity: " + message + "\n");
                            }
                        }
                        break;
                    case 3:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync("https://localhost:44359/api/parking/freePlaces");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nFreePlaces: " + message + "\n");
                            }
                        }
                        break;
                    case 4:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync("https://localhost:44359/api/vehicles");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nAll vehicles: \n" + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 5:
                        using (HttpClient client = new HttpClient())
                        {
                            Console.WriteLine("Write id of vehicle: ");
                            string id = Console.ReadLine();
                            var response = await client.GetAsync($"https://localhost:44359/api/vehicles/ + {id}");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\n\nVehicle: " + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 6:
                        using (HttpClient client = new HttpClient())
                        {
                            Console.WriteLine("\nWrite id of new vehicle: ");
                            string vehicleId = Console.ReadLine();
                            Console.WriteLine("\nWrite type of vehicle: ");
                            int type = Int32.Parse(Console.ReadLine());
                            Console.WriteLine("\nWrite balance: ");
                            decimal vehicleBalance = Decimal.Parse(Console.ReadLine());
                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, $"https://localhost:44359/api/vehicles");
                            requestMessage.Content = JsonContent.Create(new { id = vehicleId, balance = vehicleBalance, vehicleType = type });
                            var response = await client.SendAsync(requestMessage);
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nNew vehicle: " + message);
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 7:
                        using (HttpClient client = new HttpClient())
                        {
                            Console.WriteLine("Write id of vehicle: ");
                            string id = Console.ReadLine();
                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Delete, $"https://localhost:44359/api/vehicles/ + {id}");
                            var response = await client.SendAsync(requestMessage);
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                Console.WriteLine("\nVehicle was sucessfully deleted!\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }

                        }
                        break;
                    case 8:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync($"https://localhost:44359/api/transactions/last");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\n\nLast transaction: " + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 9:
                        using (HttpClient client = new HttpClient())
                        {
                            var response = await client.GetAsync($"https://localhost:44359/api/transactions/all");
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\n\nAll transactions: " + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 10:
                        using (HttpClient client = new HttpClient())
                        {
                            Console.WriteLine("\nWrite id of vehicle: ");
                            string vehicleId = Console.ReadLine();
                            Console.WriteLine("\nWrite sum: ");
                            decimal vehicleSum = Decimal.Parse(Console.ReadLine());
                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Put, $"https://localhost:44359/api/transactions/topUpVehicle");
                            requestMessage.Content = JsonContent.Create(new { id = vehicleId, sum = vehicleSum });
                            var response = await client.SendAsync(requestMessage);
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string message = await response.Content.ReadAsStringAsync();
                                Console.WriteLine("\nUpdated vehicle: " + message + "\n");
                            }
                            else
                            {
                                Console.WriteLine($"Responce error code: {response.StatusCode}");
                            }
                        }
                        break;
                    case 0:
                        flag = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
